# DiffRename

An IntelliJ IDEA plugin to rename large numbers of files with a diff, allowing for the usage of regex or other bulk edit methods.
