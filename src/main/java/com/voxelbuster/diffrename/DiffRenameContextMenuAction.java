package com.voxelbuster.diffrename;

import com.intellij.diff.DiffManager;
import com.intellij.diff.DiffRequestFactory;
import com.intellij.diff.requests.ContentDiffRequest;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.actionSystem.ActionUpdateThread;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
DiffRename
    Copyright (C) 2022  Galen Nare

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

public class DiffRenameContextMenuAction extends AnAction {
    private static final Logger log = Logger.getInstance(DiffRenameContextMenuAction.class);

    private final DiffRequestFactory diffRequestFactory = DiffRequestFactory.getInstance();
    private final LocalFileSystem localFileSystem = LocalFileSystem.getInstance();
    private final DiffManager diffManager = DiffManager.getInstance();

    private static final TreeMap<String, String> renameFiles = new TreeMap<>();

    @Override
    public void update(@NotNull AnActionEvent e) {
        super.update(e);
    }

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        Project currentProject = e.getProject();

        VirtualFile[] virtualFiles = CommonDataKeys.VIRTUAL_FILE_ARRAY.getData(e.getDataContext());

        if (virtualFiles != null) {
            List<String> filesSelected =
                Stream.of(virtualFiles)
                    .map(VirtualFile::getPath)
                    .collect(Collectors.toList());

            try {
                File srcTmpNamesFile = Files.createTempFile("diffRenameSrc", System.currentTimeMillis() + ".txt")
                        .toFile();
                File destTmpNamesFile = Files.createTempFile("diffRenameDest", System.currentTimeMillis() + ".txt")
                        .toFile();
                String dest = Objects.requireNonNull(localFileSystem.findFileByIoFile(destTmpNamesFile)).getPath();
                String src = Objects.requireNonNull(localFileSystem.findFileByIoFile(srcTmpNamesFile)).getPath();
                renameFiles.put(dest, src);

                writeFilenames(filesSelected, srcTmpNamesFile);
                writeFilenames(filesSelected, destTmpNamesFile);

                ContentDiffRequest diffRequest =
                        diffRequestFactory.createFromFiles(currentProject,
                                localFileSystem.findFileByIoFile(srcTmpNamesFile),
                                localFileSystem.findFileByIoFile(destTmpNamesFile));

                diffManager.showDiff(currentProject, diffRequest);

            } catch (IOException ex) {
                log.error("Error creating temp file:", ex);
                Messages.showErrorDialog("Failed to create temp file.", "Diff Rename");
            }
        }
    }

    private void writeFilenames(List<String> filesSelected, File destTmpNamesFile) throws IOException {
        FileWriter destWriter = new FileWriter(destTmpNamesFile);
        filesSelected.forEach(path -> {
            try {
                destWriter.write(path);
                destWriter.write(System.lineSeparator());
            } catch (IOException ex) {
                log.error("Error writing file names to disk:", ex);
            }
        });
        destWriter.close();
    }

    public static boolean hasDestFile(String fname) {
        return renameFiles.containsKey(fname);
    }

    @SuppressWarnings("unused")
    static void applyChanges(String srcContent, String destContent, Project p) {
        List<String> srcLines = List.of(srcContent.split("\\n"));
        List<String> destLines = List.of(destContent.split("\\n"));

        if (srcLines.size() != destLines.size()) {
            int response = Messages.showYesNoDialog("There are not the same number of source paths as destination" +
                    "paths, do you want to continue? The process will stop at whichever list is shorter.",
                    "Diff Rename", AllIcons.General.WarningDialog);
            if (response != Messages.YES) {
                return;
            }
        }

        for (int idx = 0; idx < Math.min(srcLines.size(), destLines.size()); idx++) {
            String srcTargetPath = srcLines.get(idx);
            String destTargetPath = destLines.get(idx);
            try {
                Files.move(Path.of(srcTargetPath), Path.of(destTargetPath));
            } catch (IOException e) {
                Messages.showErrorDialog(String.format("Failed to move %s to %s",
                        srcTargetPath, destTargetPath), "Diff Rename");
                log.error("Failed to commit file move action:", e);
            }

            VirtualFileManager.getInstance().refreshWithoutFileWatcher(true);
        }
    }

    @Override
    public @NotNull ActionUpdateThread getActionUpdateThread() {
        return ActionUpdateThread.BGT;
    }
}
