package com.voxelbuster.diffrename;

import com.intellij.diff.DiffManager;
import com.intellij.diff.DiffRequestFactory;
import com.intellij.diff.chains.SimpleDiffRequestChain;
import com.intellij.diff.contents.FileDocumentContentImpl;
import com.intellij.diff.editor.ChainDiffVirtualFile;
import com.intellij.diff.requests.ContentDiffRequest;
import com.intellij.diff.requests.SimpleDiffRequest;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/*
DiffRename
    Copyright (C) 2022  Galen Nare

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
public class DiffTabCloseListener implements FileEditorManagerListener {
    @Override
    public void fileClosed(@NotNull FileEditorManager source, @NotNull VirtualFile file) {
        FileEditorManagerListener.super.fileClosed(source, file);

//        System.out.println(file.getPath());

        if (file.getPath().equals("/Diff") && file instanceof ChainDiffVirtualFile cdvFile) {
            List<SimpleDiffRequestChain.DiffRequestProducerWrapper> requests =
                    (List<SimpleDiffRequestChain.DiffRequestProducerWrapper>) cdvFile.getChain().getRequests();

            if (!requests.isEmpty()) {
                VirtualFile srcFile = ((FileDocumentContentImpl) ((SimpleDiffRequest) requests.get(0).getRequest())
                        .getContents().get(0)).getFile();
                VirtualFile destFile = ((FileDocumentContentImpl) ((SimpleDiffRequest) requests.get(0).getRequest())
                        .getContents().get(1)).getFile();

                String srcContent = ((FileDocumentContentImpl) ((SimpleDiffRequest) requests.get(0).getRequest())
                        .getContents().get(0)).getDocument().getText();
                String destContent = ((FileDocumentContentImpl) ((SimpleDiffRequest) requests.get(0).getRequest())
                        .getContents().get(1)).getDocument().getText();

                if (DiffRenameContextMenuAction.hasDestFile(destFile.getPath())) {
                    int response = Messages.showYesNoCancelDialog(source.getProject(), "Apply changes to file paths?",
                            "Diff Rename", "Apply", "Continue Edits", "Cancel", AllIcons.General.QuestionDialog);
                    if (response == Messages.YES) {
                        DiffRenameContextMenuAction.applyChanges(srcContent, destContent, source.getProject());
                    } else if (response == Messages.NO) {
                        ContentDiffRequest diffRequest =
                                DiffRequestFactory.getInstance().createFromFiles(source.getProject(), srcFile, destFile);

                        DiffManager.getInstance().showDiff(source.getProject(), diffRequest);
                    }
                }
            }
        }
    }
}
